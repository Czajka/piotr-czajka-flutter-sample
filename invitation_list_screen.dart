import 'package:xyz/data/model/event.dart';
import 'package:xyz/screen/invitation/invitation_card.dart';
import 'package:xyz/screen/invitation/invitation_list_screen_bloc.dart';
import 'package:xyz/screen/invitation/invitation_empty_view.dart';
import 'package:xyz/screen/invitation/invitation_loader.dart';
import 'package:xyz/style/app_color.dart';
import 'package:xyz/utils/status_bar_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:inject/inject.dart';

class InvitationListScreen extends StatefulWidget {
  final InvitationListScreenBloc _invitationBloc;

  @provide
  InvitationListScreen(this._invitationBloc) : super();

  @override
  _InvitationListScreenState createState() => _InvitationListScreenState();
}

class _InvitationListScreenState extends State<InvitationListScreen> {
  final ScrollController _scrollController = ScrollController();
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();

  List<Event> events = List();

  @override
  void initState() {
    super.initState();
    _initScrollController();
    _observeEvent();
    widget._invitationBloc.loadEvents();
  }

  _initScrollController() {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        widget._invitationBloc.loadLaterEvents(events);
      }
    });
  }

  _observeEvent() {
    _observeRefreshEvents();
    _observeLaterEvents();
    _observeRefreshError();
  }

  _observeRefreshEvents() => widget._invitationBloc.observeRefreshEvents.listen(
      (refreshEvents) => refreshEvents.isNotEmpty
          ? _insertRefreshedEvents(refreshEvents)
          : setState(() => events = List.of(refreshEvents)));

  _observeLaterEvents() => widget._invitationBloc.observeLaterEvents
      .listen((laterEvents) => _insertLaterEvents(laterEvents));

  _observeRefreshError() =>
      widget._invitationBloc.refreshError.listen(() => {}); //TODO: handle error message

  @override
  Widget build(BuildContext context) {
    StatusBar.setColor(AppColor.colorPrimaryDark);
    return new Scaffold(
        body: SafeArea(
            child: RefreshIndicator(
                child: Stack(
                  children: <Widget>[
                    _invitationList(context),
                    _observeEmptyInvitationsListViewVisibility(),
                  ],
                ),
                onRefresh: _refreshEvents)));
  }

  Future<dynamic> _refreshEvents() async => widget._invitationBloc.loadEvents();

  _observeEmptyInvitationsListViewVisibility() => LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) {
          return SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: viewportConstraints.maxHeight,
                ),
                child: StreamBuilder(
                    stream: widget._invitationBloc.emptyInvitationsViewVisible,
                    initialData: false,
                    builder:
                        (BuildContext context, AsyncSnapshot<bool> snapshot) {
                      return Visibility(
                          child: InvitationEmptyView(), visible: snapshot.data);
                    })),
          );
        },
      );

  _invitationList(BuildContext context) => Container(
      color: AppColor.white,
      child: AnimatedList(
          key: _listKey,
          controller: _scrollController,
          initialItemCount: events.length + 1,
          itemBuilder: (context, position, animation) => position >=
                  events.length
              ? InvitationPagingLoader(widget._invitationBloc,
                  onRetry: () => widget._invitationBloc.loadLaterEvents(events))
              : _buildInvitationCard(
                  position, events, widget._invitationBloc, animation)));

  _buildInvitationCard(int position, List<Event> events,
          InvitationListScreenBloc bloc, Animation animation) =>
      SizeTransition(
        axis: Axis.vertical,
        sizeFactor: animation,
        child: InvitationCard(position, events, widget._invitationBloc),
      );

  _insertRefreshedEvents(List<Event> newEvents) {
    _deleteAllEvents();

    events = List.of(newEvents);
    _insertEvents(newEvents);
  }

  _insertLaterEvents(List<Event> laterEvents) {
    events.addAll(laterEvents);
    _insertEvents(laterEvents, insertIndex: events.indexOf(events.last));
  }

  _insertEvents(List<Event> newEvents, {int insertIndex = 0}) {
    for (int itemOffset = 0; itemOffset < newEvents.length; itemOffset++) {
      _listKey.currentState.insertItem(insertIndex + itemOffset);
    }
  }

  _deleteAllEvents() {
    if (_animatedStateListContainItems()) {
      events.forEach((evt) => _listKey.currentState.removeItem(
          0,
          (context, animation) => InvitationCard(
              events.indexOf(evt), events, widget._invitationBloc),
          duration: Duration(seconds: 0)));
    }
  }

  _animatedStateListContainItems() => events.length > 0;

  _deleteEvent(Event event) {
    //TODO: implement delete event
  }

  @override
  void dispose() {
    _scrollController.dispose();
    widget._invitationBloc.dispose();
    super.dispose();
  }
}
