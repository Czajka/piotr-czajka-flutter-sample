import 'package:xyz/data/model/event.dart';

abstract class EventRepository {
  Future<List<Event>> getEvents({String fromDate = ""});

  Future<bool> removeEvent(String eventId);
}
