import 'package:xyz/data/http/events_api.dart';
import 'package:xyz/data/mapper/events_mapper.dart';
import 'package:xyz/data/model/event.dart';
import 'package:xyz/data/network_client.dart';
import 'package:fimber/fimber.dart';
import 'package:inject/inject.dart';

class EventsApiProvider implements EventsApi {
  NetworkClient networkClient;

  @provide
  EventsApiProvider(this.networkClient);

  @override
  Future<List<Event>> getEvents({String fromParam = ""}) async {
    try {
      final response =
          await networkClient.getDio().get("events/" + fromParam.toString());
      return EventsMapper.mapToList(response.data);
    } catch (error) {
      return _handleError(error);
    }
  }

  @override
  Future<bool> removeEvent(String eventId) async {
    try {
      final response =
          await networkClient.getDio().get("events/" + eventId.toString());
      return response.statusCode == 200;
    } catch (error) {
      return _handleError(error);
    }
  }

  _handleError(error) {
    Fimber.e(this.runtimeType.toString(), ex: error);
    return Future.error(error);
  }
}
