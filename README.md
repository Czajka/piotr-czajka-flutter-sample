This sample contains event cards list implementation with pull to refresh and pagination. 
It was created using BLoC pattern. We can find here each layer implementation:

**View:**  
invitation_list_screen.dart  
invitation_card.dart  

**BLoC component:**  
invitation_list_screen_bloc.dart  

**Repository:**  
event_repository.dart  
event_repository_impl.dart  

**Remote DataSource:**  
events_api.dart  
events_api_provider.dart