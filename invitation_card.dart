import 'package:xyz/data/model/attendee.dart';
import 'package:xyz/data/model/event.dart';
import 'package:xyz/generated/i18n.dart';
import 'package:xyz/screen/invitation/attendees_dialog.dart';
import 'package:xyz/screen/invitation/attendees_list_view.dart';
import 'package:xyz/screen/invitation/event_popup_menu.dart';
import 'package:xyz/screen/invitation/invitation_list_screen_bloc.dart';
import 'package:xyz/style/app_color.dart';
import 'package:xyz/style/app_image_assets.dart';
import 'package:xyz/utils/common_const.dart';
import 'package:xyz/utils/date_format.dart';
import 'package:xyz/utils/image_loader.dart';
import 'package:xyz/utils/string_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xyz/utils/common_const.dart' as CommonConst;

class InvitationCard extends StatelessWidget {
  final int _position;
  final InvitationListScreenBloc _invitationBloc;
  final List<Event> _events;

  InvitationCard(this._position, this._events, this._invitationBloc);

  @override
  Widget build(BuildContext context) {
    return _invitationContent(context, _position);
  }

  _invitationContent(BuildContext context, int position) => Container(
        margin: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 18.0),
        child: Card(
            elevation: 8.0,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
            child: _checkInvitationType(context, position)),
      );

  _checkInvitationType(BuildContext context, int position) => _isOwner(position)
      ? _invitationOwner(context, position)
      : _invitationParticipant(context, position);

  _isOwner(int position) =>
      _invitationBloc.getUser().id == _events[_position].owner.id;

  _invitationOwner(BuildContext context, int position) => Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(14.0, 14.0, 14.0, 2.0),
            child: Row(
              children: <Widget>[
                _checkPlaceIcon(_events[_position]),
                Expanded(
                  child: Text(_events[_position].name,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.title),
                ),
                EventPopUpMenuWidget(true, position,
                    onRemove: (pos) =>
                        _invitationBloc.deleteEvent(_events[pos])),
              ],
            ),
          ),
          _checkAddress(context, _events[_position]),
          Padding(
            padding: EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 14.0),
            child: _eventTime(context),
          ),
          _cardImage(),
          _attendeesList(context),
          _showInvitationButton(context, _events[_position])
        ],
      );

  _invitationParticipant(BuildContext context, int position) => Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(14.0, 14.0, 14.0, 8.0),
            child: Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 10.0),
                  child: ImageLoader.loadProfileImage(
                      _events[_position].owner.imageUrl),
                ),
                Expanded(
                  child: Text(_events[_position].owner.name,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.body2),
                ),
                EventPopUpMenuWidget(false, position),
              ],
            ),
          ),
          Stack(
            fit: StackFit.passthrough,
            children: <Widget>[
              Container(child: _cardImage()),
              Container(
                  margin: EdgeInsets.only(top: cardImageHeight - fabSize / 2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      _fabButton(AppImageAssets.fab_check_grey, () => {}),
                      Container(
                          child: _fabButton(
                              AppImageAssets.fab_close_grey, () => {}),
                          margin: EdgeInsets.only(left: 16.0, right: 16.0)),
                    ],
                  ))
            ],
          ),
          Container(
              padding: EdgeInsets.only(left: 14.0, right: 14.0, top: 10.0),
              child: Row(
                children: <Widget>[
                  _checkPlaceIcon(_events[_position]),
                  Expanded(
                    child: Text(_events[_position].name,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.title),
                  )
                ],
              )),
          _checkAddress(context, _events[_position]),
          Padding(
            padding: EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 14.0),
            child: _eventTime(context),
          ),
          Divider(color: AppColor.gray),
          _attendeesList(context),
          _showInvitationButton(context, _events[_position])
        ],
      );

  _placeGrayIcon() => Image.asset(
        AppImageAssets.place_grey,
        fit: BoxFit.contain,
        height: CommonConst.placeIconHeight,
        width: CommonConst.placeIconWidth,
      );

  _eventTime(BuildContext context) => Text(
      DateUtils.readTimestamp(context, _events[_position].eventTime),
      style: Theme.of(context).textTheme.subhead.copyWith(color: AppColor.red));

  _cardImage() =>
      Image.asset(AppImageAssets.background, height: 160, fit: BoxFit.cover);

  _attendeesList(BuildContext context) => Container(
      margin: EdgeInsets.fromLTRB(0.0, 12.0, 0.0, 8.0),
      child: AttendeesListView(_events[_position].attendees, false));

  _checkPlaceIcon(Event event) => Visibility(
      child: Container(
          child: _placeGrayIcon(), margin: EdgeInsets.only(right: 10.0)),
      visible: StringUtils.isNotNullOrEmpty(event.googlePlaceId));

  _checkAddress(BuildContext context, Event event) =>
      StringUtils.isNullOrEmpty(event.address)
          ? Container()
          : Padding(
              padding: EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 0.0),
              child: Text(event.address,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context)
                      .textTheme
                      .body2
                      .copyWith(color: AppColor.black54)),
            );

  _fabButton(String imageAssetPath, VoidCallback onPressed) => Container(
        height: fabSize,
        width: fabSize,
        child: FittedBox(
            child: FloatingActionButton(
                onPressed: () => onPressed,
                backgroundColor: AppColor.white,
                child: Image.asset(imageAssetPath))),
      );

  _showInvitationButton(BuildContext context, Event event) =>
      event.attendees.length > maxVisibleAttendees
          ? _showAllButton(context, event.attendees)
          : _inviteButton(context);

  _inviteButton(BuildContext context) =>
      _invitationFlatButton(context, S.of(context).invite, () => {});

  _showAllButton(BuildContext context, List<Attendee> attendees) =>
      _invitationFlatButton(context, S.of(context).showAll,
          () => _showAllAttendees(context, attendees));

  _invitationFlatButton(
      BuildContext context, String name, VoidCallback onPressed) {
    return FlatButton(
        padding:
            EdgeInsets.only(left: 14.0, bottom: 16.0, top: 16.0, right: 14.0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0.0)),
        splashColor: AppColor.controlHighlight,
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        child: SizedBox(
            width: double.infinity,
            child: Text(
              name,
              style: Theme.of(context)
                  .textTheme
                  .button
                  .copyWith(color: AppColor.red),
            )),
        onPressed: onPressed);
  }

  _showAllAttendees(BuildContext context, List<Attendee> attendees) =>
      showDialog(
          context: context,
          builder: (BuildContext context) => AttendeesDialog(attendees));
}
