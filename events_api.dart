import 'package:xyz/data/model/event.dart';

abstract class EventsApi {
  Future<List<Event>> getEvents({String fromParam});

  Future<bool> removeEvent(String eventId);
}
