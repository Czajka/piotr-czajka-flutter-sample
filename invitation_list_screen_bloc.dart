import 'package:xyz/data/model/event.dart';
import 'package:xyz/data/repository/event/event_repository.dart';
import 'package:xyz/data/user/user.dart';
import 'package:xyz/data/user/user_repository.dart';
import 'package:xyz/utils/empty_event.dart';
import 'package:inject/inject.dart';
import 'package:rxdart/rxdart.dart';

class InvitationListScreenBloc {
  final EventRepository _eventRepository;
  final _refreshEvents = PublishSubject<List<Event>>();
  final _laterEvents = PublishSubject<List<Event>>();
  final _pagingLoaderState = PublishSubject<PagingLoaderState>();
  final _emptyInvitationViewVisible = PublishSubject<bool>();
  final User _user;
  final refreshError = EmptyEvent();
  bool _noMoreLaterEvents;

  Observable<List<Event>> get observeRefreshEvents => _refreshEvents.stream;

  Observable<List<Event>> get observeLaterEvents => _laterEvents.stream;

  Observable<bool> get emptyInvitationsViewVisible =>
      _emptyInvitationViewVisible.stream;

  Observable<PagingLoaderState> get pagingLoaderState =>
      _pagingLoaderState.stream;

  @provide
  InvitationListScreenBloc(this._eventRepository, UserRepository userRepository)
      : _user = userRepository.getUser();

  loadEvents() async {
    _noMoreLaterEvents = false;
    _requestForEvents();
  }

  _requestForEvents() async =>
      await _eventRepository.getEvents().then((events) => _handleEvents(events),
          onError: (error) => _handleEventsError());

  _handleEvents(List<Event> events) {
    if (events.isEmpty) {
      _showEmptyListView();
    } else {
      _hideEmptyListView();
      _refreshEvents.sink.add(events);
    }
  }

  _handleEventsError() {
    refreshError.pushEmptyEvent();
  }

  loadLaterEvents(List<Event> events) async {
    if (events.isNotEmpty && !_noMoreLaterEvents) {
      _pushPagingLoaderState(PagingLoaderState.showPagingLoader);
      _requestForLaterEvents(events);
    }
  }

  _requestForLaterEvents(List<Event> events) async => await _eventRepository
      .getEvents(fromDate: events.last.eventTime.toString())
      .then((laterEvent) => _handleLaterEvents(laterEvent),
          onError: (error) => _handleLaterEventsError());

  _handleLaterEvents(List<Event> laterEvents) {
    _pushPagingLoaderState(PagingLoaderState.hidePagingLoader);
    _noMoreLaterEvents = laterEvents.isEmpty;
    if (!_noMoreLaterEvents) {
      _laterEvents.sink.add(laterEvents);
    }
  }

  _handleLaterEventsError() {
    _noMoreLaterEvents = false;
    _pushPagingLoaderState(PagingLoaderState.showPagingLoaderFailed);
  }

  _pushPagingLoaderState(PagingLoaderState state) =>
      _pagingLoaderState.sink.add(state);

  _showEmptyListView() => _emptyInvitationViewVisible.sink.add(true);

  _hideEmptyListView() => _emptyInvitationViewVisible.sink.add(false);

  User getUser() => _user;

  deleteEvent(Event event) => _requestForDeleteEvent(event);

  _requestForDeleteEvent(Event event) async =>
      _eventRepository.removeEvent(event.id).then((_) => {});

  void dispose() {
    _refreshEvents.close();
    _laterEvents.close();
    _pagingLoaderState.close();
    _emptyInvitationViewVisible.close();
    refreshError.close();
  }
}

enum PagingLoaderState {
  showPagingLoader,
  hidePagingLoader,
  showPagingLoaderFailed
}
