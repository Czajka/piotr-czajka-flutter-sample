import 'package:xyz/data/http/events_api.dart';
import 'package:xyz/data/model/event.dart';
import 'package:xyz/data/repository/event/event_repository.dart';

class EventRepositoryImpl implements EventRepository {
  EventsApi _eventsApi;

  EventRepositoryImpl(this._eventsApi);

  @override
  Future<List<Event>> getEvents({String fromDate = ""}) async =>
      _eventsApi.getEvents(fromParam: fromDate);

  @override
  Future<bool> removeEvent(String eventId) async =>
      _eventsApi.removeEvent(eventId);
}
